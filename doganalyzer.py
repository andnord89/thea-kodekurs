from filereader import FileReader
from Dog import Dog

class DogAnalyzer(object):

  def __init__(self):
    self.dogs = []

  def loadDogsFromFile(self, filename):
    fr = FileReader()
    for values in fr.getValuesFromFile("dogs.txt"):
      newDog = Dog(values[0], values[1], values[2], values[3], values[4])
      self.dogs.append(newDog)


  # complete this function
  def countDogsOfColor(self, color):
    return 0


  # complete this function
  def getAverageDogAge(self):
    return 0
  

  def showDogs(self):
    for dog in self.dogs:
      print('Dog:  age={0},  age={1},  kind={2},  color={3}'.format(dog.name, dog.age, dog.kind, dog.color))
    print("\n")


doganalyzer = DogAnalyzer()
doganalyzer.loadDogsFromFile("dogs.txt")
doganalyzer.showDogs()


color = "brown"
numDogsOfColor = doganalyzer.countDogsOfColor(color)
print('Number of Dogs with the color {0}: {1}\n'.format(color, numDogsOfColor))

avgDogAge = doganalyzer.getAverageDogAge()
print('Average age of dogs: {0}\n'.format(avgDogAge))


#https://www.coursera.org/professional-certificates/ibm-data-science#courses